package le1779.hw2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.HashMap;

import le1779.hw2.model.http.Constants;
import le1779.hw2.model.http.HttpListener;
import le1779.hw2.model.http.OkHttpManager;
import le1779.hw2.model.json.JsonUtils;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void okHttpExample(){
        String url = "https://api";
        HashMap<String, String> params = new HashMap<>();
        params.put("key", "value");
        OkHttpManager.getInstance().requestServerData(this, Constants.HTTP_GET_METHOD, url, params, new HttpListener() {
            @Override
            public void Successful(String message) {
            }

            @Override
            public void Fault(String message) {
            }
        });
    }

    private void gsonExample(){
        String json = "";
        Type type = new TypeToken<Result<Person>>() {}.getType();
        Result result = JsonUtils.deserialize(json, type);
    }

    private class Result<T> implements Serializable {

        @SerializedName("status")
        private int status;
        @SerializedName("Data")
        private T Data;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public T getData() {
            return Data;
        }

        public void setData(T data) {
            Data = data;
        }
    }

    private class Person implements Serializable{

        @SerializedName("id")
        private String id;
        @SerializedName("name")
        private String name;
        @SerializedName("age")
        private int age;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }
}
