package le1779.hw2.model.http;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.Request;

public class CommonRequest {
    private static final String TAG = "CommonRequest";
    public static Request createGetRequest(@NonNull String baseUrl, HashMap<String, String> params) {
        StringBuilder urlBuilder = new StringBuilder(baseUrl).append("?");
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                urlBuilder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
            }
        }
        Log.d(TAG, urlBuilder.toString());
        return new Request.Builder().get().url(urlBuilder.substring(0, urlBuilder.length() - 1)).build();
    }

    public static Request createPostRequest(@NonNull String baseUrl, HashMap<String, String> params) {
        FormBody.Builder mFormBodyBuilder = new FormBody.Builder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            mFormBodyBuilder.add(entry.getKey(), entry.getValue());
        }
        FormBody formBody = mFormBodyBuilder.build();
        return new Request.Builder().post(formBody).url(baseUrl).build();
    }
}
