package le1779.hw2.model.json;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;

/**
 * Created by Kevin on 2018/3/12.
 */

public class JsonUtils {
    private static Gson gson = new Gson();

    public static <T> String serialize(T object) {
        return gson.toJson(object);
    }

    public static <T> T deserialize(String json, Type type) throws JsonSyntaxException {

        return gson.fromJson(json, type);
    }

    public static <T> T deserialize(JsonElement json, Type type) throws JsonSyntaxException {

        return gson.fromJson(json, type);
    }
}
