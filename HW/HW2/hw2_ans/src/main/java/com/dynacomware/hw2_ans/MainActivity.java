package com.dynacomware.hw2_ans;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dynacomware.hw2_ans.model.data.TaipeiWeather.Result;
import com.dynacomware.hw2_ans.model.data.TaipeiWeather.Weather;
import com.dynacomware.hw2_ans.model.http.Constants;
import com.dynacomware.hw2_ans.model.http.HttpListener;
import com.dynacomware.hw2_ans.model.http.OkHttpManager;
import com.dynacomware.hw2_ans.model.json.JsonUtils;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private EditText editText_location;
    private TextView textView_result;
    private static final String url_taipei_weather = "https://data.taipei/opendata/datalist/apiAccess";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText_location = findViewById(R.id.editText_location_search);
        textView_result = findViewById(R.id.textView_result);
    }

    public void search(View view){
        String location = String.valueOf(editText_location.getText());

        HashMap<String, String> params = new HashMap<>();
        params.put("scope", "resourceAquire");
        params.put("rid", "1f1aaba5-616a-4a33-867d-878142cac5c4");
        params.put("q", location);

        OkHttpManager.getInstance().requestServerData(this, Constants.HTTP_GET_METHOD, url_taipei_weather, params, new HttpListener() {
            @Override
            public void Successful(String message) {
                Intent intent = new Intent(MainActivity.this, WeatherActivity.class);
                intent.putExtra("Result", analysisJson(message));
                startActivity(intent);
            }

            @Override
            public void Fault(String message) {
                showResult("連接失敗");
            }
        });
    }

    private void showResult(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView_result.setText(message);
            }
        });
    }

    private Result analysisJson(String json){
        JsonObject jsonObject = JsonUtils.deserialize(json, JsonObject.class);
        Result result = JsonUtils.deserialize(jsonObject.get("result"), new TypeToken<Result<Weather>>() {}.getType());
        return result;
    }
}
