package com.dynacomware.hw2_ans;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.dynacomware.hw2_ans.model.data.TaipeiWeather.Result;
import com.dynacomware.hw2_ans.model.data.TaipeiWeather.Weather;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WeatherActivity extends AppCompatActivity {

    private RecyclerView recyclerView_weather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        Intent intent = getIntent();
        Result result = (Result) intent.getSerializableExtra("Result");
        List<Weather> weathers = result.getResults();

        recyclerView_weather = findViewById(R.id.recyclerView_weather);
        recyclerView_weather.setAdapter(new WeatherAdapter(this, weathers));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView_weather.setLayoutManager(layoutManager);
    }
}
