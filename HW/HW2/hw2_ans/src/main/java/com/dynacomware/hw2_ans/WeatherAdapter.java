package com.dynacomware.hw2_ans;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dynacomware.hw2_ans.model.data.TaipeiWeather.Weather;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ItemViewHolder> {

    private Context context;
    private List<Item> items;
    private List<Weather> weatherList;
    private int index = 0;

    public WeatherAdapter(Context context, List<Weather> weatherList){
        this.context = context;
        this.weatherList = weatherList;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Map<String, Map<String, List<Weather>>> weatherMap = new HashMap<>();
        for(Weather weather : weatherList){
            Map<String, List<Weather>> tempMap = new HashMap<>();
            String district = weather.getLocationName();
            if(weatherMap.containsKey(district)){
                tempMap = weatherMap.get(district);
            }
            String date = dateFormat.format(weather.getDataTime());
            List<Weather> tempList = new ArrayList<>();
            if(tempMap.containsKey(date)){
                tempList = tempMap.get(date);
            }
            tempList.add(weather);
            tempMap.put(date, tempList);
            weatherMap.put(district, tempMap);
        }

        items = new ArrayList<>();
        for(Map.Entry<String, Map<String, List<Weather>>> entry : weatherMap.entrySet()) {
            TitleItem titleItem =  new TitleItem(entry.getKey());
            items.add(titleItem);
            index++;
            for(Map.Entry<String, List<Weather>> weather : entry.getValue().entrySet()){
                WeatherItem weatherItem = new WeatherItem();
                weatherItem.date = weather.getKey();
                weatherItem.weathers = weather.getValue();
                items.add(weatherItem);
                index++;
            }
        }

//        Map<String, List<Weather>> districtWeather = new HashMap<>();
//        for(Weather weather : weatherList){
//            List<Weather> temp = new ArrayList<>();
//            String district = weather.getLocationName();
//            if(districtWeather.containsKey(district)){
//                temp = districtWeather.get(district);
//            }
//            temp.add(weather);
//            districtWeather.put(district, temp);
//        }
//
//        items = new ArrayList<>();
//        for(Map.Entry<String, List<Weather>> entry : districtWeather.entrySet()) {
//            TitleItem titleItem =  new TitleItem(entry.getKey());
//            items.add(titleItem);
//            for(Weather weather : entry.getValue()){
//                WeatherItem weatherItem = new WeatherItem(weather);
//                items.add(weatherItem);
//            }
//        }
    }

    @NonNull
    @Override
    public WeatherAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if(viewType == 0){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_weather_list_title, parent, false);
            return new TitleViewHolder(view);
        }else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_weather_list_content, parent, false);
            return new WeatherViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherAdapter.ItemViewHolder holder, int position) {
        Item item = items.get(position);
        switch (getItemViewType(position)) {
            case 0:
                TitleViewHolder titleViewHolder = (TitleViewHolder) holder;
                TitleItem titleItem = (TitleItem) item;
                titleViewHolder.textView_title.setText(titleItem.location);
                break;
            case 1:
                WeatherViewHolder weatherViewHolder = (WeatherViewHolder) holder;
                WeatherItem weatherItem = (WeatherItem) item;
                DateFormat dateFormat = new SimpleDateFormat("HH:MM:ss");
                String content = weatherItem.date + "\n";
                for(Weather weather : weatherItem.weathers){
                    View view =LayoutInflater.from(context).inflate(R.layout.item_time, null);
                    TextView textView_time = view.findViewById(R.id.textView_time);
                    TextView textView_temperature = view.findViewById(R.id.textView_temperature);
                    textView_time.setText(dateFormat.format(weather.getDataTime()));
                    textView_temperature.setText(weather.getMeasures() + ": " + weather.getValue());
                    weatherViewHolder.linearLayout_time.addView(view);
                }
                //String content = dateFormat.format(weatherItem.weather.getDataTime()) + " ," + weatherItem.weather.getMeasures() + ": " + weatherItem.weather.getValue() + "\n";
                weatherViewHolder.textView_content.setText(content);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return index;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    private enum ItemType{
        TYPE_GROUP(0), TYPE_CHILD(1);

        private final int value;

        ItemType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private class TitleItem extends Item {
        public String location;

        public TitleItem(String location){
            this.location = location;
        }

        @Override
        public int getType() {
            return ItemType.TYPE_GROUP.getValue();
        }
    }

    private class WeatherItem extends Item {
        public String date;
        public List<Weather> weathers;

        @Override
        public int getType() {
            return ItemType.TYPE_CHILD.getValue();
        }
    }

    private abstract class Item {
        public abstract int getType();
    }

    public class TitleViewHolder extends ItemViewHolder {

        private TextView textView_title;

        TitleViewHolder(View itemView) {
            super(itemView);
            textView_title = itemView.findViewById(R.id.textView_title);
        }

        @Override
        public int getType() {
            return ItemType.TYPE_GROUP.getValue();
        }
    }

    public class WeatherViewHolder extends ItemViewHolder {

        private TextView textView_content;
        private LinearLayout linearLayout_time;

        WeatherViewHolder(View v) {
            super(v);
            textView_content = v.findViewById(R.id.textView_content);
            linearLayout_time = v.findViewById(R.id.linearLayout_time);
        }

        @Override
        public int getType() {
            return ItemType.TYPE_CHILD.getValue();
        }
    }

    abstract class ItemViewHolder extends RecyclerView.ViewHolder {
        private ItemViewHolder(View itemView) {
            super(itemView);
        }

        public abstract int getType();
    }
}
