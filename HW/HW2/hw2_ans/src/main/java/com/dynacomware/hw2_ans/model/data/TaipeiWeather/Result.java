package com.dynacomware.hw2_ans.model.data.TaipeiWeather;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Result<T> implements Serializable {

    @SerializedName("limit")
    private int limit;
    @SerializedName("offset")
    private int offset;
    @SerializedName("count")
    private int count;
    @SerializedName("sort")
    private String sort;
    @SerializedName("results")
    private List<T> results;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }
}
