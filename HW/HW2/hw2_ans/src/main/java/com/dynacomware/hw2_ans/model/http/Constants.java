package com.dynacomware.hw2_ans.model.http;

public class Constants {
    public final static int HTTP_GET_METHOD = 1;
    public final static int HTTP_POST_METHOD = 2;
    public final static int HTTP_PUT_METHOD = 3;
    public final static int HTTP_TIME_OUT = 30;

}
