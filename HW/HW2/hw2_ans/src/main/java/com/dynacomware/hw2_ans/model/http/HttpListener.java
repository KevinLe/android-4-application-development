package com.dynacomware.hw2_ans.model.http;

/**
 * Created by admin on 2018/3/12.
 */

public interface HttpListener {
    void Successful(String message);
    void Fault(String message);
}
