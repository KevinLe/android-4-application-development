package le1779.android4applicationdevelopment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import le1779.android4applicationdevelopment.model.Person;

public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void goToSecondActivity(View view){
        sendDataByObject();
    }

    private void sendFewData(){
        String value = "test";
        Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
        intent.putExtra("key", value);
        intent.putExtra("key_int", 123);
        startActivity(intent);
    }

    private void sendDataByBundle(){
        Bundle bundle = new Bundle();
        bundle.putString("key", "value");
        bundle.putInt("key_int", 132);

        Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void sendDataByObject(){
        Person person = new Person();
        person.setId("A123456789");
        person.setName("Kevin");
        person.setAge(22);

        Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
        intent.putExtra("person", person);
        startActivity(intent);
    }

    private void sendDataAndResult(){
        Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
        int requestCode = 0;
        startActivityForResult(intent ,requestCode);
    }
}
