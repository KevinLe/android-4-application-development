package le1779.android4applicationdevelopment;

import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class IntentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent);


    }

    public void changePage(View view){
        startActivity(new Intent(this, FirstActivity.class));
        finish();
    }

    public void search(View view){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY,"searchString");
        startActivity(intent);
    }

    public void browser(View view){
        Uri uri = Uri.parse("http://www.google.com");
        Intent intent  = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public void map(View view){
        Uri uri = Uri.parse("geo:38.899533,-77.036476");
        Intent intent = new Intent(Intent.ACTION_VIEW,uri);
        startActivity(intent);
    }
}
