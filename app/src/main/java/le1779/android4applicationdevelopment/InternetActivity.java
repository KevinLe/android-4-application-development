package le1779.android4applicationdevelopment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import le1779.android4applicationdevelopment.model.Person;
import le1779.android4applicationdevelopment.model.http.Constants;
import le1779.android4applicationdevelopment.model.http.HttpListener;
import le1779.android4applicationdevelopment.model.http.OkHttpManager;
import le1779.android4applicationdevelopment.model.json.JsonUtils;
import le1779.android4applicationdevelopment.model.json.TaipeiWeather.Result;
import le1779.android4applicationdevelopment.model.json.TaipeiWeather.Weather;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class InternetActivity extends AppCompatActivity {

    private TextView textView_network_status, textView_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internet);

        textView_network_status = findViewById(R.id.textView_network_status);
        textView_result = findViewById(R.id.textView_result);
    }

    public void checkNetWork(View view){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            //Connected
            textView_network_status.setText("Connected");
        } else {
            //Not Connected
            textView_network_status.setText("Not Connected");
        }
    }

    public void getTaipeiWeather(View view){
        usedThread();
    }

    public void getTaipeiWeatherByOkHttp(View view){
        //getDataByOkHttp();

        String url = "https://data.taipei/opendata/datalist/apiAccess";
        HashMap<String, String> params = new HashMap<>();
        params.put("scope", "resourceAquire");
        params.put("rid", "1f1aaba5-616a-4a33-867d-878142cac5c4");
        params.put("q", "內湖區");
        OkHttpManager.getInstance().requestServerData(this, Constants.HTTP_GET_METHOD, url, params, new HttpListener() {
            @Override
            public void Successful(String messenger) {
                JsonObject jsonObject = JsonUtils.deserialize(messenger, JsonObject.class);

                Result result = JsonUtils.deserialize(jsonObject.get("result"), new TypeToken<Result<Weather>>() {}.getType());
                List<Weather> weathers = result.getResults();
                Map<String, List<Weather>> districtWeather = new HashMap<>();
                for(Weather weather : weathers){
                    List<Weather> temp = new ArrayList<>();
                    String district = weather.getLocationName();
                    if(districtWeather.containsKey(district)){
                        temp = districtWeather.get(district);
                    }
                    temp.add(weather);
                    districtWeather.put(district, temp);
                }
                String showStr = "";
                for(Map.Entry<String, List<Weather>> map : districtWeather.entrySet()){
                    showStr += map.getKey() + "\n";
                    for(Weather weather : map.getValue()){
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH");
                        showStr += dateFormat.format(weather.getDataTime()) + " ," + weather.getMeasures() + ": " + weather.getValue() + "\n";
                    }
                }
                showResult(showStr);
            }

            @Override
            public void Fault(String messenger) {

            }
        });
    }

    private void showResult(final String messenger){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView_result.setText(messenger);
            }
        });
    }

    private void usedThread(){
        String url = "https://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=1f1aaba5-616a-4a33-867d-878142cac5c4&q=內湖區";
        try {
            textView_result.setText(new NetWorkAsyncTask().execute(url).get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    class NetWorkAsyncTask extends AsyncTask<String, Integer, String>{
        @Override
        protected String doInBackground(String... strings) {
            StringBuilder response = new StringBuilder();
            for(String param : strings){
                try{
                    URL url = new URL(param);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    InputStream is = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                        response.append('\r');
                    }
                    reader.close();
                }catch (Exception e){

                }
            }
            return response.toString();
        }
    }

    private void getDataByOkHttp() {
        Request request = new Request.Builder()
                .url("https://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=1f1aaba5-616a-4a33-867d-878142cac5c4&q=內湖區")
                .build();
        new OkHttpClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    showResult(response.body().string());
                    response.body().close();
                }
            }
        });
    }

    private void postDataByOkHttp(){
        FormBody.Builder mFormBodyBuilder = new FormBody.Builder();
        mFormBodyBuilder.add("Key", "Value");
        FormBody formBody = mFormBodyBuilder.build();
        Request request = new Request.Builder()
                .url("https://api.com")
                .post(formBody)
                .build();
        new OkHttpClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    showResult(response.body().string());
                    response.body().close();
                }
            }
        });
    }

    private void json(String jsonStr){
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            String id = jsonObj.getString("id");
            String name = jsonObj.getString("name");
            int age = jsonObj.getInt("age");
            Person person = new Person();
            person.setId(id);
            person.setName(name);
            person.setAge(age);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Gson gson = new Gson();
        JsonObject jsonObject = gson.fromJson(jsonStr, JsonObject.class);
        Type type = new TypeToken<Result<Weather>>() {}.getType();
        Result result = gson.fromJson(jsonObject.get("result"), type);
        List<Weather> weathers = result.getResults();
    }
}
