package le1779.android4applicationdevelopment;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ResourceActivity extends AppCompatActivity {

    private TextView textView_file_content;
    private Button button_read_raw, button_read_assets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource);

        textView_file_content = findViewById(R.id.textView_file_content);

        button_read_raw = findViewById(R.id.button_read_raw);
        button_read_raw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputStream inputStream = getResources().openRawResource(R.raw.raw_file);
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder fileContent = new StringBuilder();
                try {
                    while(reader.ready()) {
                        fileContent.append(reader.readLine());
                    }
                    textView_file_content.setText(fileContent.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        button_read_assets = findViewById(R.id.button_read_assets);
        button_read_assets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    AssetManager am = getAssets();
                    InputStream inputStream = am.open("assets_file");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuilder fileContent = new StringBuilder();
                    while(reader.ready()) {
                        fileContent.append(reader.readLine());
                    }
                    textView_file_content.setText(fileContent.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
