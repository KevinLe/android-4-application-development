package le1779.android4applicationdevelopment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import le1779.android4applicationdevelopment.model.Person;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        TextView textView_test = findViewById(R.id.textView_test);
        textView_test.setText(getDataByObject());
    }

    private String getFewData(){
        Intent intent = getIntent();
        int integer = intent.getIntExtra("key_int", 0);
        String string = intent.getStringExtra("key");
        return "integer: "+ integer + ", string: " + string;
    }

    private String getDataByBundle(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String string = bundle.getString("key");
        int integer = bundle.getInt("key_int");
        return "integer: "+ integer + ", string: " + string;
    }

    private String getDataByObject(){
        Intent intent = getIntent();
        Person person = (Person) intent.getSerializableExtra("person");
        return "ID: " + person.getId() + " ,Name: " + person.getName() + " ,Age: " + person.getAge();
    }

    private void sendResult(){
        Intent intent = getIntent();
        int resultCode = 0;
        setResult(resultCode, intent);
    }
}
