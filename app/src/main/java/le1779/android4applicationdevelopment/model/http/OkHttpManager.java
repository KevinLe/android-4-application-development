package le1779.android4applicationdevelopment.model.http;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;


import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OkHttpManager {

    private static volatile OkHttpManager sManager;
    private OkHttpClient mOkHttpClient;

    private OkHttpManager() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
        builder.connectTimeout(Constants.HTTP_TIME_OUT, TimeUnit.SECONDS);
        builder.readTimeout(Constants.HTTP_TIME_OUT, TimeUnit.SECONDS);
        builder.writeTimeout(Constants.HTTP_TIME_OUT, TimeUnit.SECONDS);
        builder.followRedirects(true);
        mOkHttpClient = builder.build();
    }


    public static OkHttpManager getInstance() {
        if (sManager == null) {
            synchronized (OkHttpManager.class) {
                if (sManager == null) {
                    sManager = new OkHttpManager();
                }
            }
        }
        return sManager;
    }

    /**
     * 使用{@link OkHttpClient}請求數據
     * @param method {@link Constants#HTTP_GET_METHOD} Get方式,{@link Constants#HTTP_POST_METHOD} Post方式
     * @param baseUrl Url
     * @param paramsMap 參數
     * @param httpListener 監聽
     */
    public void requestServerData(Context context, int method, String baseUrl, HashMap<String, String> paramsMap, final HttpListener httpListener) {
        if(!checkNetwork(context)){
            httpListener.Fault("no network");
            return;
        }
        Request request = null;
        switch (method){
            case Constants.HTTP_GET_METHOD:
                request = CommonRequest.createGetRequest(baseUrl, paramsMap);
                break;
            case Constants.HTTP_POST_METHOD:
                request = CommonRequest.createPostRequest(baseUrl, paramsMap);
                break;
        }

        if (request != null) {
            mOkHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    httpListener.Fault(e.toString());
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    try {
                        httpListener.Successful(response.body().string());
                        response.body().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

   private boolean checkNetwork(Context context){
       ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
       NetworkInfo networkInfoe = cm != null ? cm.getActiveNetworkInfo() : null;
       return networkInfoe != null;
   }
}
