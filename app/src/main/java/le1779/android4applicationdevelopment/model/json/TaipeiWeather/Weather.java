package le1779.android4applicationdevelopment.model.json.TaipeiWeather;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Weather implements Serializable {

    @SerializedName("dataTime")
    private Date dataTime;
    @SerializedName("measures")
    private String measures;
    @SerializedName("lon")
    private String lon;
    @SerializedName("value")
    private String value;
    @SerializedName("locationName")
    private String locationName;
    @SerializedName("geocode")
    private String geocode;
    @SerializedName("_full_count")
    private String _full_count;
    @SerializedName("rank")
    private double rank;
    @SerializedName("lat")
    private double lat;
    @SerializedName("_id")
    private String _id;

    public Date getDataTime() {
        return dataTime;
    }

    public void setDataTime(Date dataTime) {
        this.dataTime = dataTime;
    }

    public String getMeasures() {
        return measures;
    }

    public void setMeasures(String measures) {
        this.measures = measures;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getGeocode() {
        return geocode;
    }

    public void setGeocode(String geocode) {
        this.geocode = geocode;
    }

    public String get_full_count() {
        return _full_count;
    }

    public void set_full_count(String _full_count) {
        this._full_count = _full_count;
    }

    public double getRank() {
        return rank;
    }

    public void setRank(double rank) {
        this.rank = rank;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
