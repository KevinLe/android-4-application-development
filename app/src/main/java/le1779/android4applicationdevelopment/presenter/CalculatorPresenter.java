package le1779.android4applicationdevelopment.presenter;


import le1779.android4applicationdevelopment.presenter.contract.CalculatorContract;

public class CalculatorPresenter implements CalculatorContract.Presenter {

    private CalculatorContract.View view;
    private String tempNum = "";
    private String currentNum = "";
    private boolean nextNum = false;
    private char perOprator = '\0';

    public CalculatorPresenter(CalculatorContract.View view){
        this.view = view;
        view.showResult(0);
    }

    @Override
    public void inputNum(char num) {
        currentNum += num;
        view.showResult(Integer.parseInt(currentNum));
    }

    @Override
    public void inputOperator(char operator) {
        if(tempNum == null || "".equals(tempNum)){
            tempNum = currentNum;
            perOprator = operator;
            currentNum = "";
            view.showResult(0);
        }else{
            switch (operator){
                case '+':
                case '-':
                case '*':
                case '/':
                    if(perOprator != '\0'){
                        count();
                    }
                    perOprator = operator;
                    view.showResult(Integer.parseInt(tempNum));
                    break;
                case '=':
                    count();
                    view.showResult(Integer.parseInt(tempNum));
                    currentNum = "";
                    perOprator = '\0';
                    break;
                case 'c':
                    tempNum = "";
                    currentNum = "";
                    perOprator = '\0';
                    view.showResult(0);
                    break;
            }
        }
    }

    private void count(){
        if("".equals(currentNum) || currentNum == null){
            return;
        }
        int num1 = Integer.parseInt(tempNum);
        int num2 = Integer.parseInt(currentNum);
        int result = 0;
        switch (perOprator){
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
        }
        tempNum = String.valueOf(result);
        currentNum = "";
    }
}
