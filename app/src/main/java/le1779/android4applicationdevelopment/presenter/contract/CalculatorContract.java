package le1779.android4applicationdevelopment.presenter.contract;

public interface CalculatorContract {
    interface View{
        void showResult(int result);
    }
    interface Presenter{
        void inputNum(char num);
        void inputOperator(char operator);
    }
}
