package le1779.android4applicationdevelopment.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import le1779.android4applicationdevelopment.R;
import le1779.android4applicationdevelopment.presenter.CalculatorPresenter;
import le1779.android4applicationdevelopment.presenter.contract.CalculatorContract;

public class CalculatorActivity extends AppCompatActivity implements View.OnClickListener, CalculatorContract.View {

    private CalculatorPresenter calculatorPresenter;
    private TextView textView_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        initView();
        calculatorPresenter = new CalculatorPresenter(this);
    }

    @Override
    public void showResult(int result) {
        textView_result.setText(String.valueOf(result));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_0:
                calculatorPresenter.inputNum('0');
                break;
            case R.id.button_1:
                calculatorPresenter.inputNum('1');
                break;
            case R.id.button_2:
                calculatorPresenter.inputNum('2');
                break;
            case R.id.button_3:
                calculatorPresenter.inputNum('3');
                break;
            case R.id.button_4:
                calculatorPresenter.inputNum('4');
                break;
            case R.id.button_5:
                calculatorPresenter.inputNum('5');
                break;
            case R.id.button_6:
                calculatorPresenter.inputNum('6');
                break;
            case R.id.button_7:
                calculatorPresenter.inputNum('7');
                break;
            case R.id.button_8:
                calculatorPresenter.inputNum('8');
                break;
            case R.id.button_9:
                calculatorPresenter.inputNum('9');
                break;
            case R.id.button_add:
                calculatorPresenter.inputOperator('+');
                break;
            case R.id.button_sub:
                calculatorPresenter.inputOperator('-');
                break;
            case R.id.button_mult:
                calculatorPresenter.inputOperator('*');
                break;
            case R.id.button_divi:
                calculatorPresenter.inputOperator('/');
                break;
            case R.id.button_ac:
                calculatorPresenter.inputOperator('c');
                break;
            case R.id.button_equal:
                calculatorPresenter.inputOperator('=');
                break;
        }
    }

    private void initView(){
        textView_result = findViewById(R.id.textView_result);
        initNumButton();
        initOperatorButton();
    }

    private void initNumButton(){
        Button button_0 = findViewById(R.id.button_0);
        Button button_1 = findViewById(R.id.button_1);
        Button button_2 = findViewById(R.id.button_2);
        Button button_3 = findViewById(R.id.button_3);
        Button button_4 = findViewById(R.id.button_4);
        Button button_5 = findViewById(R.id.button_5);
        Button button_6 = findViewById(R.id.button_6);
        Button button_7 = findViewById(R.id.button_7);
        Button button_8 = findViewById(R.id.button_8);
        Button button_9 = findViewById(R.id.button_9);

        button_0.setOnClickListener(this);
        button_1.setOnClickListener(this);
        button_2.setOnClickListener(this);
        button_3.setOnClickListener(this);
        button_4.setOnClickListener(this);
        button_5.setOnClickListener(this);
        button_6.setOnClickListener(this);
        button_7.setOnClickListener(this);
        button_8.setOnClickListener(this);
        button_9.setOnClickListener(this);
    }

    private void initOperatorButton(){
        Button button_add = findViewById(R.id.button_add);
        Button button_sub = findViewById(R.id.button_sub);
        Button button_mult = findViewById(R.id.button_mult);
        Button button_divi = findViewById(R.id.button_divi);
        Button button_ac = findViewById(R.id.button_ac);
        Button button_equal = findViewById(R.id.button_equal);

        button_add.setOnClickListener(this);
        button_sub.setOnClickListener(this);
        button_mult.setOnClickListener(this);
        button_divi.setOnClickListener(this);
        button_ac.setOnClickListener(this);
        button_equal.setOnClickListener(this);
    }

}
